<%@page import="java.net.URLEncoder"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

	<link rel="stylesheet" type="text/css" href="bootstrap.css">
       	
	<!-- Initialize the FB JS SDK -->
	<script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '772225696217123',
	      xfbml      : true,
	      version    : 'v2.5'
	    });
	  };
	
	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
		 js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=772225696217123";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
	
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>Welcome to likes predictor</title>
</head>
<body>
	<div class="container">
		<div
		  class="fb-like"
		  data-share="true"
		  data-width="450"
		  data-show-faces="true">
		</div>
	
		<h1 class="jumbotron">CRAZY LIKES PREDICTOR</h1>
	
		<!--
		  Below we include the Login Button social plugin. This button uses
		  the JavaScript SDK to present a graphical Login button that triggers
		  the FB.login() function when clicked.
		-->
		
		<!-- <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
		</fb:login-button> -->
	
	<!-- 
		<div id="status">
		</div>
	 -->
	<!-- 	<div class="row col-md-3 col-md-offset-4">
		 	<div class="fb-login-button" data-max-rows="3" data-size="medium"
				 data-show-faces="true" data-auto-logout-link="true">
			</div>
		</div>
	 --> 
	 	<a class=" col-md-offset-4 btn btn-primary btn-lg" onclick="
			FB.login(function(response) {
				  if (response.status === 'connected') {
				    console.log(response.authResponse.accessToken);
				    window.location = '../likespredictor?auth='+
				    		response.authResponse.accessToken;
				  } else if (response.status === 'not_authorized') {
					  console.log('Not Authorized!');
				  } else {
					  console.log('Sth else!');
				  }
				}, {scope: 'user_posts'});
		">Connect with Facebook</a>	
	</div>
</body>
</html>