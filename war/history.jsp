<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="bootstrap.css">
	<link rel="stylesheet" type="text/css" href="custom.css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Previous Posts Prediction</title>
	
	<!-- Initialize the FB JS SDK -->
	<script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '772225696217123',
	      xfbml      : true,
	      version    : 'v2.5'
	    });
	  };
	
	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
		 js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=772225696217123";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
</head>

<body>

		<!-- 'predictions' variable has ArrayList of Prediction objects -->	
		<!-- 'predByCat' variable has ArrayList containing 3 items:
				0->m_predict; 1->e_predict; 2->s_predict -->
	
	<div class="container">
	
		<h1 class="lead lead_text">Your previous posts predicted</h1>
		
		<div class="posts">
			<c:forEach items="${predictions}" var="entry">
      	   		<div class="panel panel-default">	
					<div class="panel-header">
	          	   	<c:if test="${entry.category eq 1}" > 
  					  <h2 class="panel-title">Status Update</h2>
	              	</c:if>	
	              	<c:if test="${entry.category eq 3}" > 
  					  <h2 class="panel-title">Media Post</h2>
	              	</c:if>	
 					<c:if test="${entry.category eq 2}" > 
  					  <h2 class="panel-title">External Link</h2>
	              	</c:if>	
 					</div>
         	   		<div class="panel-body">
	              	<c:if test="${entry.category eq 1}" > 
  					  <p class="">"<c:out value="${entry.message}"/></h2>
	              	</c:if>	
	              	<c:if test="${entry.category eq 3}" > 
 					  <img src="<c:out value="${entry.message}"/>" />
	              	</c:if>
	              	<c:if test="${entry.category eq 2}" > 
 					  <a href="<c:out value="${entry.message}"/>">Link To Post</a>
	              	</c:if>
 					</div>
 					<div class="panel-footer">
	          		<p class="btn btn-md btn-success">Predicted:<c:out value="${entry.predictedLikes}"/></p>
	          		<p class="btn btn-md btn-warning">Actual:<c:out value="${entry.actualLikes}"/></p>
 					</div>
         		</div>
             </c:forEach>
		</div>
	</div>
	
</body>
</html>