<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="bootstrap.css">
	<link rel="stylesheet" type="text/css" href="custom.css">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<!-- Initialize the FB JS SDK -->
	<script>
	  window.fbAsyncInit = function() {
	    FB.init({
	      appId      : '772225696217123',
	      xfbml      : true,
	      version    : 'v2.5'
	    });
	  };
	
	  (function(d, s, id){
	     var js, fjs = d.getElementsByTagName(s)[0];
	     if (d.getElementById(id)) {return;}
	     js = d.createElement(s); js.id = id;
		 js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=772225696217123";
	     fjs.parentNode.insertBefore(js, fjs);
	   }(document, 'script', 'facebook-jssdk'));
	</script>
	
<title>Merlin's Beard</title>
</head>
<body>
	
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
	
	
	<div class="container">
	
  		<div class="col-md-1 col-md-offset-4">
		 	<div class="fb-login-button" data-max-rows="3" data-size="medium"
				 data-show-faces="true" data-auto-logout-link="true">
			</div>
		</div>
 		
 		<h3 class="lead lead_text">Information about your profile</h3>
 		
		<div class="row">
			
			<div id="likes_table" class="col-md-3">
				<table class = "table">
				   <caption>Top Likers</caption>
	
				   <thead>
				      <tr>
				         <th>Name</th>
				         <th>Count</th>
				      </tr>
				   </thead>
				   
				   <tbody>
		               <c:forEach items="${map}" var="entry">
		                   	<tr class="info">
		                   	  <c:if test="${entry.key != 'EXTERNAL LINKS' and
		                   	   entry.key != 'STATUS UPDATES' and
		                   	   entry.key != 'MEDIA'}" >
			                  	<td><c:out value="${entry.key}"/></td>
			                  	<td><c:out value="${entry.value}"/></td>
			                  </c:if>	
		               		</tr>                    
		               </c:forEach>
				   </tbody>
				</table>	
			</div>		
			
			<div id="pie_chart" class="col-md-9">
			
				<script src="d3.min.js" type="text/javascript"></script>
		
		 		<script type="text/javascript" >
		 		
		 		
					 console.log(${PCobj});
					// Start creating a Pie Chart	
					(function(d3) { 	 		  
					var dataset = ${PCobj};
					 
					var width = 800,
					    height = 540,
					    radius = Math.min(width, height) / 2;
					
					var color = d3.scale.ordinal()
					    .range(['#A60F2B', '#648C85', '#B3F2C9', '#528C18', '#C3F25C',"#38abc5","#98bbc5","#98abc5", "#8a89a6", "#7b6888", "#6b486b", "#a05d56", "#d0743c", "#ff8c00"]);
					
					var arc = d3.svg.arc()
					    .outerRadius(radius - 10)
					    .innerRadius(0);
					
					var name_arc = d3.svg.arc()
					    .outerRadius(radius + 250)
					    .innerRadius(0);
					
					var likes_arc = d3.svg.arc()
					   .outerRadius(radius + 150)
					   .innerRadius(0);
					
					var pie = d3.layout.pie()
					    .sort(null)
					    .value(function(d) { return d.population; });
					
					var svg = d3.select("#pie_chart").append("svg")
					    .attr("width", width)
					    .attr("height", height)
						.append("g")
					    .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");
					
					
					var arc = d3.svg.arc()
					      .outerRadius(radius);
					
					   var pie = d3.layout.pie()
					      .value(function(d) { return d.likes; })
					      .sort(null);
					
					  
					var path = svg.selectAll('path')
					      .data(pie(dataset))
					      .enter()
					      .append('path')
					      .attr('d', arc)
					      .attr('fill', function(d, i) { 
					        return color(d.data.name);
					      });
					
					
					var text = svg.selectAll('.arc')
			        .data(pie(dataset))
			        .enter()
			        .append("text")
			        .attr("transform", function(d) { return "translate(" + name_arc.centroid(d) + ")"; })
			        .attr("dy", ".35em")
			        .style("text-anchor", "start")
			        .text(function(d) { return d.data.name; });
					
					var text1 = svg.selectAll('.arc')
			        .data(pie(dataset))
			        .enter()
			        .append("text")
			        .attr("transform", function(d) { return "translate(" + likes_arc.centroid(d) + ")"; })
			        .attr("dy", ".35em")
			        .style("text-anchor", "start")
			        .text(function(d) { return d.data.likes; });
				 
				  })(window.d3);
				</script>
			
			</div>
			
		</div>

		<form method="get" action="likespredictor">
     		<input class="btn btn-success btn-lg" type="submit" id="btn1" name="history" value="See Previous Posts Predicted"/>
    	 	<input class="btn btn-primary btn-lg" type="submit" id="btn2" name="new" value="New Post Prediction"/>
		</form>
	</div>

</body>
</html>