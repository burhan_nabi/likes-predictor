<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<link rel="stylesheet" type="text/css" href="bootstrap.css">
	<link rel="stylesheet" type="text/css" href="custom.css">

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="container" id="wrap">
		
		<h2 class="lead lead_text">What will your new post have?</h2>
		
		<div  class="form-group">
		  <div class="checkbox">
		    <label>
		      <input id="t" type="checkbox"> Has Text
		    </label>
		  </div>
		  <div class="checkbox">
		    <label>
		      <input id="m" type="checkbox"> Has Media
		    </label>
		  </div>
		  <div class="checkbox">
		    <label>
		      <input id="e" type="checkbox"> Has External Link
		    </label>
		  </div>
		  <button id="submit" onclick="handleSubmit();" class="btn btn-info">Predict</button>
		</div>
	</div>
</body>
<script type="text/javascript">
	function handleSubmit() {
		var c = document.getElementById("child");
		if(c)c.parentNode.removeChild(c);
		
		var t = document.getElementById('t');
		var e = document.getElementById('e');
		var m = document.getElementById('m');
		
		var likes = 0;
		if(t.checked) {
			likes+=${s_predict};	
		}
		if(e.checked) {
			likes+=${e_predict};	
		}
		if(m.checked) {
			likes+=${m_predict};	
		}
		
		t.disabled=true;
		e.disabled=true;
		m.disabled=true;
		document.getElementById("wrap").disable = true;
		
		console.log(likes);
		var newdiv = document.createElement("h2");
		newdiv.id = "child";
		newdiv.className ="lead lead_text";
		newdiv.appendChild(document.createTextNode("Predicted Number of Likes:"+likes));
		document.getElementById("wrap").appendChild(newdiv);
	};
</script>
</html>