#LIKES PREDICTOR#
This is a [Facebook Application](http://likespred101.appspot.com/) that predicts the number of likes the user of the application can expect from any future post that he puts up.

This app is written by me as part of the never ending learning process(and for my JAVA course).

Although this is a really bad predictor, I learnt a lot an awful lot from this project.

Facebook earned my respect, atleast for keeping their data so organized.

#Screen Shots:

![Alt text](/screenshots/1.png?raw=true "")
![Alt text](/screenshots/2.png?raw=true "")
![Alt text](/screenshots/3.png?raw=true "")
