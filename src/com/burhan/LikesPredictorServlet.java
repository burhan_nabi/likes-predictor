package com.burhan;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LikesPredictorServlet extends HttpServlet {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5326741713268284854L;
	private String token;
	public static Integer s_predict=0, m_predict=0, e_predict=0;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
//		response.setContentType("text/plain");
		System.out.println("Here");
		
		
		System.out.println(request.getParameterMap().size());
		/*for (String key : request.getParameterMap().keySet()) {
			System.out.println(key);
		}*/
		
		if(request.getParameter("auth") != null) {
		
			token = request.getParameter("auth") ;
			Predictor.init( request.getParameter("auth") );
			Predictor.getToWork();
			LinkedHashMap<String , Integer> map = Predictor.getPosts();
			request.setAttribute("map", map);
			request.setAttribute("PCobj", Predictor.createDataForPieChart(map));
			request.getRequestDispatcher("results.jsp?").forward(request, response);
			System.out.println(e_predict + ":" + m_predict + ":" + s_predict);
			
		}else if(request.getParameter("history")!=null) {
			
			System.out.println("History Prediction");
			System.out.println(e_predict + ":" + m_predict + ":" + s_predict);
			Predictor.init(token);
			ArrayList<Integer> predByCat = new ArrayList<>();
			predByCat.add(m_predict);
			predByCat.add(e_predict);
			predByCat.add(s_predict);
			
			request.setAttribute("predByCat", predByCat);
			
			request.setAttribute("predictions", Predictor.getRecentPostPredictions());
			request.getRequestDispatcher("history.jsp?").forward(request, response);
		
		}else if(request.getParameter("new")!=null) {
			
			request.setAttribute("e_predict", e_predict);
			request.setAttribute("m_predict", m_predict);
			request.setAttribute("s_predict", s_predict);
			
			request.getRequestDispatcher("new_post.jsp?").forward(request, response);
			System.out.println("New Post");
		
		}else {
			request.getRequestDispatcher("Welcome.jsp").forward(request, response);
		}
	}
}
