package com.burhan;

public class Prediction {
	
	String message;
	// 1 status, 2 external link, 3 media
	int category;
	int predictedLikes, actualLikes;
	
	public Prediction(String m, int c, int p, int a) {
		message = m;
		category = c;
		predictedLikes = p;
		actualLikes = a;
	}
	public String getMessage() {
		return message;
	}
	public int getPredictedLikes() {
		return predictedLikes;
	}
	public int getActualLikes() {
		return actualLikes;
	}
	public int getCategory() {
		return category;
	}
}
