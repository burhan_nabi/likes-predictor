package com.burhan;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.google.appengine.labs.repackaged.org.json.JSONArray;
import com.google.appengine.labs.repackaged.org.json.JSONException;
import com.google.appengine.labs.repackaged.org.json.JSONObject;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.json.JsonObject;
import com.restfb.types.NamedFacebookType;
import com.restfb.types.Post;
import com.restfb.types.User;


/**
 * This class handles the prediction and is the 'brains' of the app
 */

/**
 * @author Burhan Nabi 2014031
 *
 */
public class Predictor {
	
	static String AUTH_TOKEN;
	static User user;
	static FacebookClient client;
	static PrintStream out = System.out;
	
	/**
	 * This function sets the authenticity token
	 * @param auth_token The passed token after user log-in
	 */
	
	@SuppressWarnings("deprecation")
	public static String init(String auth_token) {
		
		if(auth_token.equals(""))return null;
		
		AUTH_TOKEN = auth_token;
		client = new DefaultFacebookClient(AUTH_TOKEN);
		
		// Fetch user's name
		user = client.fetchObject("me", User.class);
		return AUTH_TOKEN;
	}

	public static void test() {
		out.println(user.getName());
	}
	
	public static void getToWork() {
		test();
		getPosts();
	}

	
	
	public static LinkedHashMap<String, Integer> getPosts() {

		// At the end of this method these structures contain the information
		// about the posts and the likers respectively.
		ArrayList<Post> posts = new ArrayList<>();
		HashMap<String, Integer> map = new HashMap<>();
		
		
		
		Connection<Post> myFeed = client.fetchConnection("me/posts", Post.class,
				Parameter.with("fields", "likes{id,name},message,attachments"));

		int total=0;
		int media=0, external=0, message=0;
		int count_m=0, count_e=0, count_s=0;
		
		for (List<Post> myFeedConnectionPage : myFeed){
			
			for (Post post : myFeedConnectionPage) {
				
//				if(post.getMessage() == null)continue;
				
				
//				out.println("Post: " + post.getMessage() );
				
				posts.add(post);
				
				
				List<NamedFacebookType> likes= new ArrayList<>();
				
//				out.println(post.getLikes());
				
				if( post.getLikes() != null) {
					
					likes =  post.getLikes().getData();
//					out.println("LIKES:" + likes.size());
					
					if(post.getMessage() == null){
						
						if(post.getAttachments() == null) {
							external+=likes.size();
							count_e++;
						}
						else {
							media=+likes.size();
							count_m++;
						}
					}else {
						if(post.getAttachments() != null) {
							media=+likes.size();
							count_m++;
						}
						message+=+likes.size();
						count_s++;
					}

					total+=likes.size();
				}
				
				if(likes!=null)
					for (NamedFacebookType like : likes) {
						
						// Populate the map
						String name = like.getName();
						if(map.get(name) == null) {
							map.put(name, new Integer(1));
						}else {
							map.put(name, map.get(name)+1);
						}
//						out.println(like.getName());
						
						
						
						
					}
				
//				out.println("*****************************");
			}
		}
		out.println("TOTAL:"+total);
		media = total - message;
		map.put("TOTAL", total);
		map.put("MEDIA",media*100/total);
		map.put("STATUS UPDATES",message*100/total);
		map.put("EXTERNAL LINKS",external*100/total);
		
		System.out.println("MEDIA:" + media);
		System.out.println("EXT:" + external);
		System.out.println("STATUS:" + message);

		
		if(count_e!=0)
			LikesPredictorServlet.e_predict = external/count_e;
		if(count_m!=0)
			LikesPredictorServlet.m_predict = media/count_m;
		if(count_s!=0) 
			LikesPredictorServlet.s_predict = message/count_s;

		return (LinkedHashMap<String, Integer>) sortByValue(map);
	}

	/**
	 * Sorts a generic map using its values as markers
	 * @param map The unsorted map
	 * @return The sorted map
	 */
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue( Map<K, V> map ){
		List<Map.Entry<K, V>> list =
		    new LinkedList<Map.Entry<K, V>>( map.entrySet() );
		Collections.sort( list, new Comparator<Map.Entry<K, V>>()
			{
			    public int compare( Map.Entry<K, V> o1, Map.Entry<K, V> o2 )
			    {
			        return (o2.getValue()).compareTo( o1.getValue() );
			    }
			} 
		);
		
		
		Map<K, V> result = new LinkedHashMap<K, V>();
		int i=0;
		for (Map.Entry<K, V> entry : list)
		{
			i++;
			if(i<14)
				result.put( entry.getKey(), entry.getValue() );
			else break;
		}		
		return result;
	}

	/**
	 * Creates JSON array of the top likers
	 * @param map The map from which to create the JSON array
	 * @return the JSONArray Object
	 */
	public static Object createDataForPieChart(LinkedHashMap<String, Integer> map) {
		JSONArray obj = new JSONArray();
		int top=0;
		
		
		// Create a JSON array of all the top likers
		for (String k : map.keySet()) {
			if(k.equals("TOTAL") || k.equals("MEDIA") || k.equals("EXTERNAL LINKS")
					|| k.equals("STATUS UPDATES")) {
				continue;
			}
			JSONObject o = new JSONObject();
			try {
				o.put("name", k);
				o.put("likes", map.get(k));
				top+=map.get(k);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			obj.put(o );
		}
		
		// Add all others likes
		JSONObject others = new JSONObject();
		try {
			Integer in = (map.get("TOTAL")-top);
			others.put("name", "Others");
			others.put("likes", in);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		obj.put(others);
		
		// Return JSON array
		return obj;
	}
	
	public static ArrayList<Prediction> getRecentPostPredictions() {
		ArrayList<Prediction> list = new ArrayList<>();
		
		JsonObject postsConnection = client.fetchObject("me/posts",
				JsonObject.class,
				Parameter.with("fields", "likes{id,name},message,attachments"),
				Parameter.with("limit", 100));
		
		System.out.println("NPOSTS:"+postsConnection.length());
		
		for (int i = 0; i < 10; i++) {
			JsonObject o ;
			try {
				o = postsConnection.getJsonArray("data").getJsonObject(i); 
				
			} catch (Exception e) {
				break;
			}
			
			// TODO Fill Predicted number of likes
			
			if(o.has("message")) {
				if(o.has("likes"))
					list.add(new Prediction(o.getString("message"),1,
							LikesPredictorServlet.s_predict, o.getJsonObject("likes").getJsonArray("data").length()
							));
			}
			else {
				if(o.has("attachments")) {
					String src;
					JsonObject jo = o.getJsonObject("attachments").
							getJsonArray("data").getJsonObject(0);
					
					if(jo.has("media")) {
					src = jo.getJsonObject("media")
							.getJsonObject("image").getString("src");
					list.add(new Prediction(src, 3, LikesPredictorServlet.m_predict,
							o.getJsonObject("likes").getJsonArray("data").length()));
					}
					else {
						src = jo.getJsonObject("target").getString("url");
						list.add(new Prediction(src, LikesPredictorServlet.e_predict, 2,
								o.getJsonObject("likes").getJsonArray("data").length()));
					}
				}	
			}
		}
		return list;
	}
	
	
}
